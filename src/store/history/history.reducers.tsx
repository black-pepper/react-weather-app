
import {INIT_HISTORY, ADD_TO_HISTORY} from './history.types'
import { IHistoryEntry } from './history.interfaces';

export const history = (state: Array<IHistoryEntry> = [], action) => {
  switch(action.type) {
    case INIT_HISTORY:
      return action.history
    case ADD_TO_HISTORY:
      return [...state, action.entry]
    default:
      return state
  }
}