import {combineReducers} from 'redux'
import {forecast} from './forecast/forecast.reducers'
import {history} from './history/history.reducers'

const rootReducer = combineReducers({history, forecast})

export default rootReducer