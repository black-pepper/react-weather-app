import {SET_FORECAST} from './forecast.types'

const initialState = {}

export const forecast = (state = initialState, action) => {
  switch(action.type) {
    case SET_FORECAST:
      return action.forecast
    default:
      return state
  }
}