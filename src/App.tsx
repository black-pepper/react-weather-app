import * as React from 'react'
import {Switch, Route, Link, withRouter} from 'react-router-dom'

import Search from './components/Search/Search'
import Weather from './components/Weather/Weather'
import Historic from './components/History/History'
// import Loader from './components/Loader'
import { css } from 'emotion'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchHistory } from './store/history/history.actions';

interface IProps {
  fetchHistory: () => void
}

class App extends React.Component<IProps> {
  componentDidMount() {
    this.props.fetchHistory()
  }
  render() {
    return (
      <div className={weatherApp}>
        <nav className={navigation}>
          <Link className={navLink} to="/">
            <div>Accueil</div>
          </Link>
          <Link className={navLink} to="/forecast">
            <div>Prévisions</div>
          </Link>
          <Link className={navLink} to="/history">
            <div>Historique</div>
          </Link>
        </nav>
        <div className={routes}>
          <Switch>
            <Route exact path="/" component={Search} />
            <Route exact path="/forecast" component={Weather} />
            <Route exact path="/history" component={Historic} />
          </Switch>
        </div>
        {/* TODO Loader <Loader loading={false} /> */}
      </div>
    )
  }
}

const navLink = css({
  padding: '10px 15px', minWidth: 'calc(100% / 3)', textAlign: 'center'
})

const weatherApp = css({
  display: 'flex',
  flexFlow: 'column nowrap',
})

const navigation = css({
  backgroundColor: '#fff',
  position: 'relative',
  zIndex: 1,
  display: 'flex',
  justifyContent: 'space-between',
  boxShadow: '1px 0 5px rgba(0,0,0,0.3)',
  '@media (max-width: 667px)': {
    position: 'fixed',
    bottom: 0,
    right: 0,
    left: 0
  }
})

const routes = css({
  minHeight: '100vh',
  '@media (max-width: 667px)': {
    order: -1
  }
})

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    fetchHistory: bindActionCreators(fetchHistory, dispatch)
  }
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default withRouter(AppContainer)