import * as React from 'react'
import { connect } from 'react-redux';
import { IHistoryEntry } from '../../store/history/history.interfaces';

interface IProps {
  history: Array<IHistoryEntry>
}

class History extends React.Component<IProps> {
  render() {
    return (
      <div>
        {this.props.history.length > 0
          ? this.props.history.map((history, i) => (
            <div key={i} style={{padding: '20px 15px', backgroundColor: '#fff'}}>{history.entry}</div>
          ))
          : 'Aucune entrée disponible'
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    history: state.history
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(History)