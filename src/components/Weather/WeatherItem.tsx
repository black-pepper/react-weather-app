import * as React from 'react'
import {css} from 'emotion'
const moment = require("moment")
import 'moment/locale/fr'

moment.locale('fr')

const date = (dateValue: string) => {
  return dateValue && <span>{moment(dateValue).format('ddd')} {moment(dateValue).format('DD/MM')}</span>
}

const hour = (dateValue: string) => {
  return `${moment(dateValue).format('HH')}h${moment(dateValue).format('mm')}`
}

export const WeatherItem = ({value}) => (
  <div className={forecastItem}>
    <div style={{textAlign: 'center', margin: '5px 0'}}>{date(value.dt_txt)}</div>
    <div className={forecastWrapper}>
      <i className={`${forecastIcon} owf owf-${value.weather[0].id}`} />
      <div>{value.weather[0].description}</div>
      <div>{`Temp. ${value.main.temp}°C`}</div>
    </div>
    <hr/>
  </div>
)

const forecastWrapper = css({
  display: 'flex',
  justifyContent: 'space-between'
})

const forecastIcon = css({
  fontSize: 36
})

const forecastItem = css({
  fontSize: 18
})

export default WeatherItem