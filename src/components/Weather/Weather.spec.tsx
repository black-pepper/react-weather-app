import * as React from 'react'
import {Weather} from './Weather'
import { shallow, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

describe('Weather component', () => {
  it('Should match snaprshot', () => {
    const props = { forecast: { list: [{}] } }
    const wrapper = shallow(<Weather {...props} />)
    expect(wrapper).toMatchSnapshot()
  })
})