import * as React from 'react'
import { connect } from 'react-redux';
import WeatherItem from './WeatherItem'
import { css } from 'emotion';

interface IWeather {}

interface IProps {
  forecast: {list: Array<IWeather>}
}

export const Weather = ({forecast}) => (
  <div>
    {forecast.list ? forecast.list.map((weatherItem, i) => (
      <WeatherItem value={weatherItem} key={i} />
    )) : 'Aucune ville sélectionnée'}
  </div>
)

const title = css({
  textAlign: "center",
  backgroundColor: '#fff',
  padding: '15px 15px'
})

const mapStateToProps = state => {
  return {
    forecast: state.forecast
  }
}

const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Weather)